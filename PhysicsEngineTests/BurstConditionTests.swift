//
//  BurstConditionTests.swift
//  Physics
//
//  Created by Zhi An Ng on 12/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import XCTest
@testable import Physics

class BurstConditionTests: XCTestCase {

    func test_burstCondition_simple() {
        let burstCondition = BurstCondition<String>()
        let grid = [
            ["*", "*", "*", "-"],
        ]
        burstCondition.grid = grid
        let results = burstCondition.determineBurst(indexPath: IndexPath(row: 0, section: 0))
        let expected = [
            IndexPath(row: 0, section: 0),
            IndexPath(row: 1, section: 0),
            IndexPath(row: 2, section: 0),
        ]
        XCTAssertEqual(results, expected)
    }

    func test_burstCondition_oddSection() {
        let burstCondition = BurstCondition<String>()
        let grid = [
            ["*", "*", "-", "-"],
            [  "*", "-", "-"],
            ["-", "-", "-", "-"],
        ]
        burstCondition.grid = grid
        let results = burstCondition.determineBurst(indexPath: IndexPath(row: 0, section: 1))
        let expected = [
            IndexPath(row: 0, section: 1),
            IndexPath(row: 0, section: 0),
            IndexPath(row: 1, section: 0),
        ]
        XCTAssertEqual(results, expected)
    }

    func test_burstCondition_evenSection() {
        let burstCondition = BurstCondition<String>()
        let grid = [
            ["-", "-", "-", "-"],
            [  "*", "-", "*"],
            ["*", "*", "*", "*"],
        ]
        burstCondition.grid = grid
        let results = burstCondition.determineBurst(indexPath: IndexPath(row: 2, section: 1))
        print(results)
        let expected = [
            IndexPath(row: 2, section: 1),
            IndexPath(row: 2, section: 2),
            IndexPath(row: 1, section: 2),
            IndexPath(row: 0, section: 1),
            IndexPath(row: 0, section: 2),
            IndexPath(row: 3, section: 2),
        ]
        XCTAssertEqual(results, expected)
    }

    func test_burstCondition_withNil() {
        let burstCondition = BurstCondition<String>()
        let grid = [
            ["-", nil, "-", "-"],
            [  "*", nil, "*"],
            ["*", "*", "*", "*"],
        ]
        burstCondition.grid = grid
        let results = burstCondition.determineBurst(indexPath: IndexPath(row: 2, section: 1))
        let expected = [
            IndexPath(row: 2, section: 1),
            IndexPath(row: 2, section: 2),
            IndexPath(row: 1, section: 2),
            IndexPath(row: 0, section: 1),
            IndexPath(row: 0, section: 2),
            IndexPath(row: 3, section: 2),
        ]
        XCTAssertEqual(results, expected)
    }

}
