//
//  PhysicsTests.swift
//  PhysicsTests
//
//  Created by Zhi An Ng on 7/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import XCTest
@testable import Physics

class PhysicsTests: XCTestCase {

    func test_world_step_velocity() {
        let world = World()
        let point = PointB()
        point.update(velocity: CGVector(dx: 0.5, dy: 0.5))
        world.add(body: point)
        for dt in Array(repeating: 10.0, count: 10) {
            world.step(dt: CGFloat(dt))
        }
        XCTAssertEqual(point.state.position.dx, 50, "Velocity simulation is wrong!")
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

}
