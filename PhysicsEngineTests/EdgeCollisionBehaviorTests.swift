//
//  EdgeCollisionBehaviorTests.swift
//  Physics
//
//  Created by Zhi An Ng on 8/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import XCTest
@testable import Physics

class EdgeCollisionBehaviorTests: XCTestCase {

    func body(x: CGFloat, y: CGFloat) -> Body {
        let b = Body()
        b.update(position: CGVector(dx: x, dy: y))
        return b
    }

    func test_collision_result_simulated() {
        let world = World()
        let bounds = CGRect(x: 0, y: 0, width: 100, height: 100)
        let edgeCollisionBehavior = EdgeCollisionBehavior(bounds: bounds)
        world.add(behavior: edgeCollisionBehavior)
        let impulseBehavior = ImpulseBehavior()
        world.add(behavior: impulseBehavior)

        // place a body at (1, 1)
        let body = self.body(x: 1, y: 1)
        // with initial vector going bottom left
        body.update(velocity: CGVector(dx: -1, dy: 1))

        world.add(body: body)

        // it should collide on the next step
        world.step(dt: 1)
        XCTAssertEqual(body.state.position, CGVector(dx: 0, dy: 2))

        // at this point the edge collisions should have been detected
        // an the impulse will have changed the vector to go bottom right
        XCTAssertEqual(body.state.velocity, CGVector(dx: 1, dy: 1))

        // now we step once more
        world.step(dt: 1)
        XCTAssertEqual(body.state.position, CGVector(dx: 1, dy: 3))

        // step until we hit the bottom edge
        world.step(dt: 97)
        XCTAssertEqual(body.state.position, CGVector(dx: 98, dy: 100))
        // then it should change velocity to go top right
        XCTAssertEqual(body.state.velocity, CGVector(dx: 1, dy: -1))

        // step until we hit the right edge
        world.step(dt: 2)
        XCTAssertEqual(body.state.position, CGVector(dx: 100, dy: 98))

        // then it should change velocity to go top left
        XCTAssertEqual(body.state.velocity, CGVector(dx: -1, dy: -1))

        // step until we hit the top edge
        world.step(dt: 98)
        XCTAssertEqual(body.state.position, CGVector(dx: 2, dy: 0))

        // then it should change velocity to go bottom left
        XCTAssertEqual(body.state.velocity, CGVector(dx: -1, dy: 1))
    }

    func test_collision_pure_behave() {
        // test collisions with each bound
        let height = 100
        let width = 100
        let world = World()
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
        let edgeCollisionBehavior = EdgeCollisionBehavior(bounds: bounds)
        world.add(behavior: edgeCollisionBehavior)

        var results: [CollisionData] = []
        NotificationCenter.default.addObserver(forName: Constants.CollisionEdge, object: nil, queue: nil, using: { n in
            results.append(n.object as! CollisionData)
        })

        let bodies: [Body] = [
            body(x: 0, y: 1), // collides with left
            body(x: 1, y: 0), // collides with top
            body(x: 100, y: 1), // collides with right
            body(x: 1, y: 100), // collies with bottom
            ]
        for body in bodies {
            world.add(body: body)
            edgeCollisionBehavior.behave(data: [])
            world.remove(body: body)
        }

        let normals = results.map { $0.normal }
        let expectedNormals = [
            CGVector(dx: -1, dy: 0),
            CGVector(dx: 0, dy: -1),
            CGVector(dx: 1, dy: 0),
            CGVector(dx: 0, dy: 1),
            ]
        XCTAssertEqual(normals, expectedNormals, "Normals for edge collisions are not correct")
    }

}
