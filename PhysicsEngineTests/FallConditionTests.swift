//
//  FallConditionTests.swift
//  Physics
//
//  Created by Zhi An Ng on 12/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import XCTest
@testable import Physics

class FallConditionTests: XCTestCase {

    func test_fallCondition_simple() {
        let fallCondition = FallCondition<String>()
        let grid = [
            [nil, nil, nil, nil],
            [  "*", "*", "*"],
        ]
        fallCondition.grid = grid
        let results = fallCondition.determineFall()
        let expected: [IndexPath] = [
            IndexPath(row: 0, section: 1),
            IndexPath(row: 1, section: 1),
            IndexPath(row: 2, section: 1),
        ]
        XCTAssertEqual(results, expected)
    }

    func test_fallCondition_heldBy_top() {
        let fallCondition = FallCondition<String>()
        let grid = [
            ["*", nil, nil, nil, nil, "*"],
            [  "*", nil, "*", nil, "*"],
        ]
        fallCondition.grid = grid
        let results = fallCondition.determineFall()
        let expected: [IndexPath] = [
            IndexPath(row: 2, section: 1),
        ]
        XCTAssertEqual(results, expected)
    }

    func test_fallCondition_heldBy_bottom() {
        let fallCondition = FallCondition<String>()
        let grid = [
            ["*", nil, nil, nil, nil, nil, nil, nil, nil, "*"],
            [  "*", nil, "*", nil, "*", nil, "*", nil, "*"],
            [nil, "*", "*", nil, nil, nil, nil, "*", "*", nil],
        ]
        fallCondition.grid = grid
        let results = fallCondition.determineFall()
        let expected: [IndexPath] = [
            IndexPath(row: 4, section: 1),
        ]
        XCTAssertEqual(results, expected)
    }

    func test_fallCondition_oddSection() {
        let fallCondition = FallCondition<String>()
        let grid = [
            ["*", nil, nil, "-"], // N Y Y N
            [  "*", "-", "-"],    //  N N N
            ["-", "-", "-", "-"], // N N N N
            [  "*", nil, nil],    //  N Y Y
            ["-", "-", "-", "-"],
            [  nil, "-", nil],
        ]
        fallCondition.grid = grid
        let results = fallCondition.determineFall()
        let expected: [IndexPath] = []
        XCTAssertEqual(results, expected)
    }

}
