//
//  BodyTests.swift
//  Physics
//
//  Created by Zhi An Ng on 7/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import XCTest
@testable import Physics

class BodyTests: XCTestCase {
    func test_pointb_bounds() {
        let p = PointB()
        p.update(position: CGVector(dx: 10, dy: 10))
        XCTAssertEqual(p.bounds, CGRect(x: 10, y: 10, width: 0, height: 0))
    }

    func test_circleb_bounds() {
        let c = CircleB(radius: 10.0)
        c.update(position: CGVector(dx: 10, dy: 10))
        XCTAssertEqual(c.bounds, CGRect(x: 0, y: 0, width: 20, height: 20))
    }
}
