//
//  BodyCollisionBehaviorTests.swift
//  Physics
//
//  Created by Zhi An Ng on 8/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import XCTest
@testable import Physics

class BodyCollisionBehaviorTests: XCTestCase {
    func circle(radius: CGFloat, x: CGFloat, y: CGFloat) -> CircleB {
        let circle = CircleB(radius: radius)
        circle.update(position: CGVector(dx: x, dy: y))
        return circle
    }

    func test_checkCollisionBetweenCircles_no_collision() {
        // position of circles
        //
        // * <- circleA
        //
        //
        //    * <- circleB
        let circleA = circle(radius: 10, x: 10, y: 10)
        let circleB = circle(radius: 5, x: 40, y: 40)
        let world = World()
        let bodyCollisionBehavior = BodyCollisionBehavior()
        world.add(behavior: bodyCollisionBehavior)
        let collisionData = bodyCollisionBehavior.checkCollisionBetween(circleA: circleA, circleB: circleB)
        XCTAssertNil(collisionData)
    }

    func test_checkCollisionBetweenCircles_touching_collision() {
        // position of circles
        // **
        // ^^-- circleB
        // |--- circleA
        let circleA = circle(radius: 10, x: 10, y: 10)
        let circleB = circle(radius: 5, x: 25, y: 10)
        let world = World()
        let bodyCollisionBehavior = BodyCollisionBehavior()
        world.add(behavior: bodyCollisionBehavior)
        let collisionData = bodyCollisionBehavior.checkCollisionBetween(circleA: circleA, circleB: circleB)
        XCTAssertNotNil(collisionData)
        XCTAssertEqual(collisionData!, CollisionData(bodyA: circleA, bodyB: circleB, normal: CGVector(dx: 0, dy: 0)))
    }

    func test_checkCollisionBetweenCircles_overlap_collision() {
        // circles overlap
        let circleA = circle(radius: 10, x: 10, y: 10)
        let circleB = circle(radius: 5, x: 20, y: 10)
        let world = World()
        let bodyCollisionBehavior = BodyCollisionBehavior()
        world.add(behavior: bodyCollisionBehavior)
        let collisionData = bodyCollisionBehavior.checkCollisionBetween(circleA: circleA, circleB: circleB)
        XCTAssertNotNil(collisionData)
        XCTAssertEqual(collisionData!, CollisionData(bodyA: circleA, bodyB: circleB, normal: CGVector(dx: 0, dy: 0)))
    }
}
