//
//  GeometryTests.swift
//  Physics
//
//  Created by Zhi An Ng on 7/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import XCTest
@testable import Physics

class GeometryTests: XCTestCase {
    func test_pointg_bounds() {
        let c = PointG()
        XCTAssertEqual(c.bounds, CGRect(x: 0, y: 0, width: 0, height: 0))
    }

    func test_circleg_bounds() {
        let c = CircleG(radius: 10)
        XCTAssertEqual(c.bounds, CGRect(x: -10, y: -10, width: 20, height: 20))
    }
}
