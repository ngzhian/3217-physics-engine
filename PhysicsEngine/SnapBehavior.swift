//
//  SnapBehavior.swift
//  Physics
//
//  Created by Zhi An Ng on 7/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import CoreGraphics
import Foundation

typealias Grid = [CGPoint]

/**
 Configure how SnapBehavior should react depending on the edge that the body collides with.
 This defaults is suitable for Bubble game.
 */
struct SnapConfig {
    var snapToTop: Bool = true
    var snapToLeft: Bool = false
    var snapToRight: Bool = false
    var snapToBottom: Bool = false
}

/**
 Pushes collided bodies to the closest grid point, *snap*.
 Reacts to "collision:edge" and "collision:body".
 */
class SnapBehavior: Behavior {

    // a grid of points to snap to
    private var grid: Grid

    // configure behavior for collisions to different edges
    private var snapConfig: SnapConfig = SnapConfig()

    // remove snap point from grid once something has snapped there
    // if this is false, bodies might overlap on the same grid point, essentially *disappearing*
    var removeSnapPointsAfterSnapping = false

    init(grid: Grid) {
        self.grid = grid
        super.init()
    }

    override func connect() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(collide(notification:)),
                                               name: Constants.CollisionEdge, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(collide(notification:)),
                                               name: Constants.CollisionBody, object: nil)
    }

    override func disconnect() {
        NotificationCenter.default.removeObserver(self, name: Constants.CollisionEdge, object: nil)
        NotificationCenter.default.removeObserver(self, name: Constants.CollisionBody, object: nil)
    }

    @objc func collide(notification: Notification) {
        switch notification.name {
        case Constants.CollisionBody:
            collideWithBody(data: notification.object as! CollisionData)
        case Constants.CollisionEdge:
            collideWithEdge(data: notification.object as! CollisionData)
        default:
            break
        }
    }

    // determines if snap should happen based on the configuration
    private func shouldSnapForEdge(edge: CollisionEdge) -> Bool {
        switch edge {
        case .Bottom:
            return self.snapConfig.snapToBottom
        case .Top:
            return self.snapConfig.snapToTop
        case .Left:
            return self.snapConfig.snapToLeft
        case .Right:
            return self.snapConfig.snapToRight
        }
    }

    func collideWithEdge(data: CollisionData) {
        guard let edge = data.edge else {
            return
        }
        if shouldSnapForEdge(edge: edge) {
            self.snap(body: data.bodyA, collisionEdge: edge)
        }
    }

    func collideWithBody(data: CollisionData) {
        self.snap(body: data.bodyA, collisionBody: data.bodyB)
        self.snap(body: data.bodyB, collisionBody: data.bodyA)
    }

    func closestPointInGrid(to point: CGPoint) -> CGPoint? {
        return self.grid.min { a, b in
            (point - a).magnitude() < (point - b).magnitude()
        }
    }

    // Actually push the body to a point.
    // If the body is static, we do nothing.
    // Otherwise set the body to be static, and set the position of the body to the snap point.
    // Afterwards, decide if we should remove the point we just snapped the body to.
    private func snap(body: Body, collisionBody: Body? = nil, collisionEdge: CollisionEdge? = nil) {
        if body.treatment == .Static {
            return
        }

        let targetPoint = CGPoint(vector: body.state.position)
        guard let snapPoint = self.closestPointInGrid(to: targetPoint) else {
            return
        }

        body.treatment = .Static
        body.state.position = CGVector(point: snapPoint)

        if removeSnapPointsAfterSnapping {
            self.grid = self.grid.filter {
                $0 != snapPoint
            }
        }

        notifySnap(body: body, snapPoint: snapPoint, collisionBody: collisionBody, collisionEdge: collisionEdge)
    }

    // Notifies listeners of the snap that just happened.
    // This allows other behaviors or the game itself to react to this logic.
    // E.g. the bubble game will know to animate the snapping and check for 3-or-more bubbles.
    private func notifySnap(body: Body, snapPoint: CGPoint, collisionBody: Body?, collisionEdge: CollisionEdge?) {
        NotificationCenter.default.post(
            name: Constants.SnapToGrid, object: SnapData(
                body: body, snapPoint: snapPoint, collisionBody: collisionBody, collisionEdge: collisionEdge))
    }

}
