//
//  Circle.swift
//  Physics
//
//  Created by Zhi An Ng on 7/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import CoreGraphics
import Foundation

class CircleG: Geometry {
    var radius: CGFloat = 0.0
    override var bounds: CGRect {
        return CGRect(x: -radius, y: -radius, width: radius * 2, height: radius * 2)
    }
    init(radius: CGFloat) {
        self.radius = radius
    }
}
