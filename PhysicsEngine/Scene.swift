//
//  Scene.swift
//  Physics
//
//  Created by Zhi An Ng on 10/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import Foundation

class Scene {
    private(set) var gameObjects: [GameObject] = []
    var physics: World

    init(physics: World) {
        self.physics = physics
    }

    func add(object: GameObject) {
        gameObjects.append(object)
        self.physics.add(body: object.body)
    }

    /// Remove from scene so will not be displayed
    func remove(object: GameObject) {
        self.physics.remove(body: object.body)
        object.isRemoved = true
    }

    /// Completely remove object to free memory
    func delete(object: GameObject) {
        self.gameObjects = self.gameObjects.filter {
            $0 !== object
        }
    }

}
