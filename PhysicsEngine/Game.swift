//
//  Game.swift
//  Physics
//
//  Created by Zhi An Ng on 10/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import CoreGraphics
import Foundation
import UIKit

class Game {
    private var size: CGSize
    private var rootView: UIView
    var physics: World
    private var scenes: [Scene] = []
    private var currentScene: Scene?
    private var renderer: GameRenderer
    private var paused: Bool = false

    // Init #1a - Designated
    init(size: CGSize, rootView: UIView, physics: World, scenes: [Scene], currentScene: Scene?, renderer: GameRenderer) {
        self.size = size
        self.rootView = rootView
        self.physics = physics
        self.scenes = scenes
        self.currentScene = currentScene
        self.renderer = renderer
    }

    init(rootView: UIView) {
        self.size = rootView.frame.size
        self.rootView = rootView
        self.physics = World()
        self.renderer = GameRenderer(rootView: rootView)
    }

    init(size: CGSize, rootView: UIView) {
        self.size = size
        self.rootView = rootView
        self.physics = World()
        self.renderer = GameRenderer(rootView: rootView)
    }

    func add(scene: Scene, setCurrentSceneToThis: Bool = false) {
        self.scenes.append(scene)
        if setCurrentSceneToThis {
            self.currentScene = scene
        }
    }

    func set(curentSceneTo scene: Scene) {
        self.currentScene = scene
    }

    // start the game loop
    func start() {
        let frameRate = 60.0
        let timeInterval = 1.0 / frameRate
        var curTime: Double = CACurrentMediaTime()
        // TODO maybe use CADisplayLink .duration
        // http://stackoverflow.com/questions/18582662/xcode-objective-c-why-is-nstimer-sometimes-slow-choppy?noredirect=1&lq=1
        // http://www.bigspaceship.com/ios-animation-intervals/

        Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: true) { _ in
            let newTime = CACurrentMediaTime()
            let elapsed = newTime - curTime
            self.physics.step(dt: CGFloat(elapsed))
            self.render()
            curTime = newTime
        }

    }

    // renders the current scene
    func render() {
        guard let scene = self.currentScene else {
            return
        }
        self.renderer.render(scene: scene)
    }
}
