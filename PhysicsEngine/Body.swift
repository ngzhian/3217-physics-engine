//
//  Body.swift
//  Physics
//
//  Created by Zhi An Ng on 7/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import CoreGraphics
import Foundation

enum Treatment {
    case Static // immovable, fixed
    case Dynamic // responds to physics
}

typealias CollisionMask = Int
let DefaultCollisionMask = 0b1

protocol PhysicsBody {
    var geometry: Geometry { get set }
    var state: State { get set }
    var oldState: State { get }
    var treatment: Treatment { get set }
    var bounds: CGRect { get set }
}

class Body {
    var geometry: Geometry = PointG()
    var state: State = State()
    var oldState: State = State()
    var treatment: Treatment = .Dynamic
    // origin is at top left
    var bounds: CGRect {
        return CGRect(x: self.geometry.bounds.minX + self.state.position.dx,
                      y: self.geometry.bounds.minY + self.state.position.dy,
                      width: self.geometry.bounds.width,
                      height: self.geometry.bounds.height)
    }
    // Decides if objects can collide if their bounds overlap
    var collisionMask = DefaultCollisionMask

    init() {
    }

    init(state: State) {
        self.state = state
    }

    init(position: CGVector, velocity: CGVector, acceleration: CGVector, angularPosition: CGFloat) {
        self.state = State(position: position,
                           velocity: velocity,
                           acceleration: acceleration,
                           angularPosition: angularPosition)
    }

    func update(position: CGVector) {
        self.oldState.position = self.state.position
        self.state.position = position
    }

    func update(velocity: CGVector) {
        self.oldState.velocity = self.state.velocity
        self.state.velocity = velocity
    }

    func update(acceleration: CGVector) {
        self.oldState.acceleration = self.state.acceleration
        self.state.acceleration = acceleration
    }

    func rotate(to radians: CGFloat) {
        self.oldState.angularPosition = self.state.angularPosition
        self.state.angularPosition = radians
    }
}
