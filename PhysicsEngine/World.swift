//
//  World.swift
//  Physics
//
//  Created by Zhi An Ng on 7/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import CoreGraphics
import Foundation
import QuartzCore

class World {
    var behaviors: [Behavior] = []
    var bodies: [Body] = []
    var stepCallback: (() -> Void)?

    func add(stepCallback: @escaping (() -> Void)) {
        self.stepCallback = stepCallback
    }

    func add(body: Body) {
        if self.bodies.contains(where: { $0 === body }) {
            return
        }
        self.bodies.append(body)
    }

    func remove(body: Body) {
        self.bodies = self.bodies.filter { $0 !== body }
    }

    func add(behavior: Behavior) {
        if self.behaviors.contains(where: { $0 === behavior }) {
            print("CONTAINS")
            return
        }
        self.behaviors.append(behavior)
        behavior.set(world: self)
    }

    func remove(behavior: Behavior) {
        guard let i = self.behaviors.index(where: { $0 === behavior }) else {
            return
        }
        let toBeRemoved = self.behaviors.remove(at: i)
        toBeRemoved.disconnect()
    }

    func step(dt: CGFloat) {
        // for each bodies, step physics logic by dt
        for body in bodies {
            updateVel(dt: dt, body: body)
            NotificationCenter.default.post(name: Constants.IntegrateVelocity, object: dt)
            updatePos(dt: dt, body: body)
            NotificationCenter.default.post(name: Constants.IntegratePosition, object: dt)
        }
        if let callback = stepCallback {
            callback()
        }
    }

    private func updateVel(dt: CGFloat, body: Body) {
        if body.treatment == .Static {
            return
        }

        let curVel = body.state.velocity
        let curAcc = body.state.acceleration
        // TODO add acceleration, it's 0 for now
        let nextVel = curVel
        // TODO change the way acc is handled
        // TODO allow external influences on acc
        body.update(velocity: nextVel)
        body.update(acceleration: curAcc)
    }

    private func updatePos(dt: CGFloat, body: Body) {
        if body.treatment == .Static {
            return
        }

        // the velocity and acceleration would have been updated in body.state
        // so we can get the previous values from body.oldState
        let curPos = body.state.position
        let curVel = body.state.velocity
        let nextPos = curPos + curVel * dt
        body.update(position: nextPos)
    }
}
