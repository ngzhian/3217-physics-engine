//
//  ImpulseBehavior.swift
//  Physics
//
//  Created by Zhi An Ng on 7/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import CoreGraphics
import Foundation

/**
 Changes a body's velocity as result of collisions.
 At the moment only calculates changes in velocity based on edge collisions.
 Reacts to "collision:edge" notifications.
 */
class ImpulseBehavior: Behavior {

    override func connect() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(collide(notification:)),
                                               name: Constants.CollisionEdge, object: nil)
    }

    override func disconnect() {
        NotificationCenter.default.removeObserver(self, name: Constants.CollisionEdge, object: nil)
    }

    @objc func collide(notification: Notification) {
        collideWithEdge(data: notification.object as! CollisionData)
    }

    func collideWithEdge(data: CollisionData) {
        if data.bodyA.treatment == .Static {
            return
        }

        let norm = data.normal
        // this is simplifed impulse simulation, we only take care of edge collisions
        // TODO we only deal with bodyA, bodyB is assumed to be Static
        if norm.dx != 0 {
            if norm.dx == 1 {
                // right, ensure velocity points to the left
                if data.bodyA.state.velocity.dx > 0 {
                    data.bodyA.update(velocity: data.bodyA.state.velocity.flipX())
                }
            } else {
                // left, ensure velocity points to the right
                if data.bodyA.state.velocity.dx < 0 {
                    data.bodyA.update(velocity: data.bodyA.state.velocity.flipX())
                }
            }
        } else {
            if norm.dy == 1 {
                // bottom, ensures velocity points up
                if data.bodyA.state.velocity.dy > 0 {
                    data.bodyA.update(velocity: data.bodyA.state.velocity.flipY())
                }
            } else {
                // top, ensures velocity points down
                if data.bodyA.state.velocity.dy < 0 {
                    data.bodyA.update(velocity: data.bodyA.state.velocity.flipY())
                }
            }
        }
    }

    override func behave(data: Any) {
        return
    }
}
