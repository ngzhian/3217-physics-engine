//
//  GameBubble.swift
//  LevelDesigner
//
//  Created by Zhi An Ng on 3/2/17.
//  Copyright © 2017 nus.cs3217.a0101010. All rights reserved.
//

import Foundation

// Simplified bubble pre-integration of PS4
class GameBubble: NSObject {
    private(set) var kind: Kind

    init(kind: Kind) {
        self.kind = kind
        super.init()
    }

    // The kind of bubble. UI uses this to choose which asset to load.
    enum Kind: Int {
        case Blue = 1
        case Red
        case Orange
        case Green
        // Clear bubble is used to LevelDesigner, and is to be displayed as a translucent bubble with outline
        case Clear
    }
}
