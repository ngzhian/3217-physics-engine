//
//  IndexPath+Extensions.swift
//  Physics
//
//  Created by Zhi An Ng on 12/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import Foundation

public struct IndexPathDelta {
    var sectionDelta: Int = 0
    var rowDelta: Int = 0
}

public func + (lhs: IndexPath, rhs: IndexPathDelta) -> IndexPath {
    return IndexPath(row: lhs.row + rhs.rowDelta, section: lhs.section + rhs.sectionDelta)
}
