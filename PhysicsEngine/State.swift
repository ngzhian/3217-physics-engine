//
//  State.swift
//  Physics
//
//  Created by Zhi An Ng on 7/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import CoreGraphics
import Foundation

struct State {
    var position: CGVector = CGVector(dx: 0, dy: 0)
    var velocity: CGVector = CGVector(dx: 0, dy: 0)
    var acceleration: CGVector = CGVector(dx: 0, dy: 0)
    // angular position in radians, positive starting along x axis
    var angularPosition: CGFloat = CGFloat(0)
}
