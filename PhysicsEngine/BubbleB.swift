//
//  BubbleB.swift
//  Physics
//
//  Created by Zhi An Ng on 12/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import CoreGraphics
import Foundation

let BlueImageFileName = "bubble-blue.png"
let RedImageFileName = "bubble-red.png"
let OrangeImageFileName = "bubble-orange.png"
let GreenImageFileName = "bubble-green.png"
let ClearImageFileName = "bubble-clear.png"
let EraseImageFileName = "erase.png"
let DefaultImageFileName = "bubble-clear.png"

let kindToFileName: [GameBubble.Kind: String] = [
    GameBubble.Kind.Blue: BlueImageFileName,
    GameBubble.Kind.Green: GreenImageFileName,
    GameBubble.Kind.Red: RedImageFileName,
    GameBubble.Kind.Orange: OrangeImageFileName,
    GameBubble.Kind.Clear: ClearImageFileName,
]

class BubbleB: CircleB {
    var indexPath: IndexPath?
    var kind: GameBubble.Kind = GameBubble.Kind.Clear
    var gameObject: GameObject?

    var imageName: String {
        return kindToFileName[self.kind] ?? DefaultImageFileName
    }

    init(indexPath: IndexPath?, radius: CGFloat, kind: GameBubble.Kind) {
        self.indexPath = indexPath
        self.kind = kind
        super.init(radius: radius)
    }
}

extension BubbleB: Equatable {
    /// Returns a Boolean value indicating whether two values are equal.
    ///
    /// Equality is the inverse of inequality. For any values `a` and `b`,
    /// `a == b` implies that `a != b` is `false`.
    ///
    /// - Parameters:
    ///   - lhs: A value to compare.
    ///   - rhs: Another value to compare.
    public static func == (lhs: BubbleB, rhs: BubbleB) -> Bool {
        return lhs.indexPath == rhs.indexPath
    }
}

extension BubbleB: Hashable {
    /// The hash value.
    ///
    /// Hash values are not guaranteed to be equal across different executions of
    /// your program. Do not save hash values to use during a future execution.
    public var hashValue: Int {
        return self.indexPath?.hashValue ?? 0
    }
}

extension BubbleB: Burstable {
    func matches(other: BubbleB) -> Bool {
        return self.kind == other.kind
    }
}
