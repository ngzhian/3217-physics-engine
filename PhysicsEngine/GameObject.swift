//
//  GameObject.swift
//  Physics
//
//  Created by Zhi An Ng on 10/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import CoreGraphics
import Foundation
import UIKit

class GameObject {
    var x: CGFloat = 0
    var y: CGFloat = 0
    var view: UIView?
    var body: Body // physics body
    var imageName: String?
    var isRemoved: Bool = false

    init(x: CGFloat, y: CGFloat, body: Body, view: UIView?, imageName: String?) {
        self.x = x
        self.y = y
        self.body = body
        self.view = view
        self.imageName = imageName
    }
}
