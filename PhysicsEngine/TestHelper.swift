//
//  TestData.swift
//  Physics
//
//  Created by Zhi An Ng on 12/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import CoreGraphics
import Foundation

struct TestHelper {
    static let rawGrid = [
        ["B", "R", "C", "B", "R", "C", "C", "C", "C", "C", "C", "C"],
        ["B", "R", "C", "B", "R", "C", "C", "C", "C", "C", "C"],
        ["C", "C", "C", "C", "C", "C", "C", "C", "C", "C", "C", "C"],
        ["C", "C", "C", "C", "C", "C", "C", "C", "C", "C", "C"],
        ["C", "C", "C", "C", "C", "C", "C", "C", "C", "C", "C", "C"],
        ["C", "C", "C", "C", "C", "C", "C", "C", "C", "C", "C"],
        ]

    static let stringToKind: [String: GameBubble.Kind] = [
        "B": GameBubble.Kind.Blue,
        "G": GameBubble.Kind.Green,
        "R": GameBubble.Kind.Red,
        "O": GameBubble.Kind.Orange,
    ]

    static func stringToBubble(radius: CGFloat, string: String, section: Int, row: Int) -> BubbleB? {
        if let kind = stringToKind[string] {
            return BubbleB(
                indexPath: IndexPath(row: row, section: section),
                radius: radius,
                kind: kind)
        } else {
            return nil
        }
    }

    static func gridToBubble(grid: [[String]], radius: CGFloat) -> [[BubbleB?]] {
        var base: [[BubbleB?]] = []
        for arr in grid {
            base.append(Array(repeating: nil, count: arr.count))
        }
        for (section, rowOfBubbles) in grid.enumerated() {
            for (row, _) in rowOfBubbles.enumerated() {
                base[section][row] = TestHelper.stringToBubble(radius: radius, string: grid[section][row], section: section, row: row)
            }
        }
        return base
    }

}
