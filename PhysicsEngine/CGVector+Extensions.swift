//
//  CGVector+Extensions.swift
//  Physics
//
//  Created by Zhi An Ng on 7/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import CoreGraphics
import Foundation

extension CGFloat {
    public var degreesInRadians: CGFloat {
        return self * CGFloat(M_PI / 180.0)
    }
}
/**
 Useful extensions to CGVector for engine
 Many credits to https://github.com/wltrup/iOS-Swift-CGExtensions/blob/master/CGExtensions.swift
 */
extension CGVector {
    public init(fromPoint: CGPoint, toPoint: CGPoint) {
        dx = toPoint.x - fromPoint.x
        dy = toPoint.y - fromPoint.y
    }

    public init(point: CGPoint) {
        dx = point.x
        dy = point.y
    }

    func flipX() -> CGVector {
        return CGVector(dx: -self.dx, dy: self.dy)
    }

    func flipY() -> CGVector {
        return CGVector(dx: self.dx, dy: -self.dy)
    }

    func magnitude() -> CGFloat {
        return sqrt(dx*dx + dy*dy)
    }

    func normalize() -> CGVector? {
        let m = magnitude()
        if m == CGFloat(0) {
            return nil
        } else {
            return CGVector(dx: dx / m, dy: dy / m)
        }
    }

    // Returns the oriented angle (in radians) from the X axis to the receiver,
    // in the semi-closed range [0, 2π). Returns 0 if the receiver is the zero
    // vector.
    public func angleFromX() -> CGFloat {
        var a = atan2(dy, dx)
        if a < 0 { a += CGFloat(360).degreesInRadians }
        assert(a >= 0 && a < CGFloat(360).degreesInRadians, "Angle not in [0, 2π)")
        return a
    }

    // Returns the oriented angle (in radians) from the Y axis to the receiver,
    // in the semi-closed range [0, 2π). Returns 3π/2 if the receiver is the
    // zero vector.
    public func angleFromY() -> CGFloat {
        let a = (angleFromX() + CGFloat(270).degreesInRadians)
            .truncatingRemainder(dividingBy: CGFloat(360).degreesInRadians)
        assert(a >= 0 && a < CGFloat(360).degreesInRadians, "Angle not in [0, 2π)")
        return a
    }
}

public func + (lhs: CGVector, rhs: CGVector) -> CGVector {
    return CGVector(dx: lhs.dx + rhs.dx, dy: lhs.dy + rhs.dy)
}

public func - (lhs: CGVector, rhs: CGVector) -> CGVector {
    return CGVector(dx: lhs.dx - rhs.dx, dy: lhs.dy - rhs.dy)
}

public func - (lhs: CGVector, rhs: CGPoint) -> CGVector {
    return CGVector(dx: lhs.dx - rhs.x, dy: lhs.dy - rhs.y)
}

public func * (lhs: CGFloat, rhs: CGVector) -> CGVector {
    return CGVector(dx: lhs * rhs.dx, dy: lhs * rhs.dy)
}

public func * (lhs: CGVector, rhs: CGFloat) -> CGVector {
    return CGVector(dx: rhs * lhs.dx, dy: rhs * lhs.dy)
}

public prefix func - (vector: CGVector) -> CGVector {
    return CGVector(dx: -vector.dx, dy: -vector.dy)
}
