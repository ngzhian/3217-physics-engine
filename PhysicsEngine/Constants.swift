//
//  Constants.swift
//  Physics
//
//  Created by Zhi An Ng on 10/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import Foundation

struct Constants {
    static let CollisionBody = Notification.Name("collision:body")
    static let CollisionEdge = Notification.Name("collision:edge")
    static let IntegrateVelocity = Notification.Name("integrate:velocity")
    static let IntegratePosition = Notification.Name("integrate:position")
    static let SnapToGrid = Notification.Name("snap:grid")
}
