//
//  CGPoint+Extensions.swift
//  Physics
//
//  Created by Zhi An Ng on 8/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import CoreGraphics
import Foundation

extension CGPoint {
    init(vector: CGVector) {
        x = vector.dx
        y = vector.dy
    }
}

public func - (lhs: CGPoint, rhs: CGPoint) -> CGVector {
    return CGVector(fromPoint: rhs, toPoint: lhs)
}
