//
//  EdgeCollisionBehavior.swift
//  Physics
//
//  Created by Zhi An Ng on 7/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import CoreGraphics
import Foundation

/**
 Checks for collisions between all bodies and the bounds.
 Emits a "collision:edge" notification.
 */
class EdgeCollisionBehavior: Behavior {
    var bounds: CGRect

    init(bounds: CGRect) {
        self.bounds = bounds
        super.init()
    }

    override func connect() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(behave(data:)),
                                               name: Constants.IntegratePosition, object: nil)
    }

    override func disconnect() {
        NotificationCenter.default.removeObserver(self, name: Constants.IntegratePosition, object: nil)
    }

    func checkCollisionBetween(body: Body, bounds: CGRect) -> [CollisionData] {
        if body.treatment == .Static {
            // don't check collision for a static body
            return []
        }

        // check all edges
        var data: [CollisionData] = []
        var overlap: CGFloat = 0
        let dummy = PointB()
        dummy.treatment = .Static

        let bodyBounds = body.bounds

        // left edge of bounds
        overlap = bounds.minX - bodyBounds.minX
        if overlap >= 0 {
            data.append(CollisionData(bodyA: body, bodyB: dummy, normal: CGVector(dx: -1, dy: 0), edge: .Left))
        }

        // right edge of bounds
        overlap = bodyBounds.maxX - bounds.maxX
        if overlap >= 0 {
            data.append(CollisionData(bodyA: body, bodyB: dummy, normal: CGVector(dx: 1, dy: 0), edge: .Right))
        }

        // top edge of bounds
        overlap = bounds.minY - bodyBounds.minY
        if overlap >= 0 {
            data.append(CollisionData(bodyA: body, bodyB: dummy, normal: CGVector(dx: 0, dy: -1), edge: .Top))
        }

        // bottom edge of bounds
        overlap = bodyBounds.maxY - bounds.maxY
        if overlap >= 0 {
            data.append(CollisionData(bodyA: body, bodyB: dummy, normal: CGVector(dx: 0, dy: 1), edge: .Bottom))
        }

        return data
    }

    override func behave(data: Any) {
        for body in self.getTargets() {
            let collisions = checkCollisionBetween(body: body, bounds: self.bounds)
            collisions.forEach {
                NotificationCenter.default.post(name: Constants.CollisionEdge, object: $0)
            }
        }
    }
}
