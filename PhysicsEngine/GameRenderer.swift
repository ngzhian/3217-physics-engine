//
//  GameRenderer.swift
//  Physics
//
//  Created by Zhi An Ng on 10/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import CoreGraphics
import Foundation
import UIKit

class GameRenderer {
    // only supports 1 scene, multiple scenes will have to clear uiview
    private var rootView: UIView

    init(rootView: UIView) {
        self.rootView = rootView
    }

    func render(scene: Scene) {
        for gameObject in scene.gameObjects {
            if let view = gameObject.view {
                if gameObject.isRemoved {
                    scene.delete(object: gameObject)
                    UIView.animate(withDuration: 0.5, animations: {
                        view.alpha = 0
                        view.frame = CGRect(origin: view.frame.origin, size: CGSize(width: 10, height: 10))
                    }, completion: {_ in
                        view.removeFromSuperview()
                    })
                    continue
                }
                view.frame = gameObject.body.bounds

                let ct = CGAffineTransform(rotationAngle: gameObject.body.state.angularPosition)
                view.transform = ct
            } else {
                var view: UIView
                if let imageName = gameObject.imageName {
                    let imageView = UIImageView(frame: gameObject.body.bounds)
                    imageView.image = UIImage(named: imageName)
                    imageView.contentMode = .scaleAspectFit
                    view = imageView
                } else {
                    view = UIView()
                    view.backgroundColor = UIColor.blue
                }
                gameObject.view = view
                rootView.addSubview(view)
            }
        }
    }
}
