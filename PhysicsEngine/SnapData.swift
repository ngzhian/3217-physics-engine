//
//  SnapData.swift
//  Physics
//
//  Created by Zhi An Ng on 10/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import CoreGraphics
import Foundation

struct SnapData {
    var body: Body
    var snapPoint: CGPoint
    var collisionBody: Body?
    var collisionEdge: CollisionEdge?
}
