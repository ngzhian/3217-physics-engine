//
//  CircleB.swift
//  Physics
//
//  Created by Zhi An Ng on 7/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import CoreGraphics
import Foundation

class CircleB: Body {
    var radius: CGFloat

    init(radius: CGFloat) {
        self.radius = radius
        super.init()
        self.geometry = CircleG(radius: radius)
    }
}
