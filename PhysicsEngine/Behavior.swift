//
//  Behavior.swift
//  Physics
//
//  Created by Zhi An Ng on 7/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import Foundation

/**
 A Behavior is describes how objects in the physics engine will react to position and velocity updates.
 Behaviors should listen to events on NotificationCenter and either
 1. update the position/velocity of the physical object
 2. emit events on NotificationCenter to be consumed by other Behavior
 */
class Behavior {
    private var world: World!
    private var targets: [Body]?

    func set(world: World) {
        self.world = world
        self.connect()
    }

    // Perform set up work after the behavior is attached to the world, e.g. listen on NotificationCenter
    func connect() {
    }

    // Perform tear down work after the behavior is removed from the world, e.g. stop listening on NotificationCtenter
    func disconnect() {
    }

    // Get a list of bodies that this behavior will apply to, by default it applies to all bodies in the world.
    func getTargets() -> [Body] {
        if self.targets != nil {
            return self.targets!
        } else {
            return self.world.bodies
        }
    }

    func applyTo(bodies: [Body]) {
        self.targets = bodies
    }

    // Called after positions have been calculated
    // Should update the position/velocity of the bodies
    @objc func behave(data: Any) {
        return
    }
}
