//
//  CollisionData.swift
//  Physics
//
//  Created by Zhi An Ng on 7/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import CoreGraphics
import Foundation

enum CollisionEdge {
    case Top
    case Right
    case Bottom
    case Left
}

/**
 Information about a collision.
 If a collision happens between a dynamic and a static body, the dynamic will be in bodyA, the static will be in bodyB.
 */
struct CollisionData {
    var bodyA: Body
    var bodyB: Body
    var normal: CGVector
    var edge: CollisionEdge?

    init(bodyA: Body, bodyB: Body, normal: CGVector, edge: CollisionEdge) {
        self.bodyA = bodyA
        self.bodyB = bodyB
        self.normal = normal
        self.edge = edge
    }

    init(bodyA: Body, bodyB: Body, normal: CGVector) {
        self.bodyA = bodyA
        self.bodyB = bodyB
        self.normal = normal
    }
}

extension CollisionData: Equatable {
    /// Returns a Boolean value indicating whether two values are equal.
    ///
    /// Equality is the inverse of inequality. For any values `a` and `b`,
    /// `a == b` implies that `a != b` is `false`.
    ///
    /// - Parameters:
    ///   - lhs: A value to compare.
    ///   - rhs: Another value to compare.
    public static func == (lhs: CollisionData, rhs: CollisionData) -> Bool {
        return lhs.bodyA === rhs.bodyA && lhs.bodyB === rhs.bodyB && lhs.normal == rhs.normal
    }
}
