//
//  FallCondition.swift
//  Physics
//
//  Created by Zhi An Ng on 12/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import Foundation

class FallCondition<T>: BaseGridCondition<T> {

    func determineFall() -> [IndexPath] {
        // DFS from first row, all marked cells won't fall
        var falling: [IndexPath] = []
        // all valid section paths in first row
        let firstSectionPaths = (0..<grid[0].count).flatMap {
            return grid[0][$0] == nil ? nil : IndexPath(row: $0, section: 0)
        }

        var explored: Set<IndexPath> = Set()
        for path in firstSectionPaths {
            self.dfs(path: path,
                     stopCondition: { _, path in explored.contains(path) },
                     action: {_, path in explored.insert(path)})
        }

        for s in 1..<grid.count {
            // all except the first section, since first section cannot fall
            for r in 0..<grid[s].count {
                let path = IndexPath(row: r, section: s)
                if grid[s][r] != nil && !explored.contains(path) {
                    falling.append(path)
                }
            }
        }

        return falling
    }

}
