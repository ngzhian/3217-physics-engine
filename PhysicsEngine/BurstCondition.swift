//
//  BurstCondition.swift
//  Physics
//
//  Created by Zhi An Ng on 12/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import Foundation

protocol Burstable: Hashable {
    // use to determine if 2 burstable can be joined and burst together
    func matches(other: Self) -> Bool
}

class BurstCondition<T: Burstable>: BaseGridCondition<T> {
    // the 2d grid to determine burst
    var minBubblesToBurst = 3

    func determineBurst(indexPath: IndexPath) -> [IndexPath] {
        var pathsToBurst: [IndexPath] = []
        let node = nodeAt(indexPath)

        guard let root = node else {
            return []
        }

        func stopCondition(node: T, path: IndexPath) -> Bool {
            return !node.matches(other: root)
        }

        func action(node: T, path: IndexPath) {
            if node.matches(other: root) {
                pathsToBurst.append(path)
            }
        }

        self.dfs(path: indexPath, stopCondition: stopCondition, action: action)

        return pathsToBurst.count >= minBubblesToBurst ? pathsToBurst : []
    }

}
