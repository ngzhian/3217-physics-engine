//
//  ViewController.swift
//  Physics
//
//  Created by Zhi An Ng on 7/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var world: World!
    var scene: Scene!
    var bubbleRadius: CGFloat!
    var bubbleWidth: CGFloat!
    var overlap: CGFloat!
    var launchVector: CGVector!
    let speedOfBubble: CGFloat = 800
    var arrow: UIView!
    let arrowCollisionMask = DefaultCollisionMask << 1
    var bubbleGrid: [[BubbleB?]] = []
    let burstCondition = BurstCondition<BubbleB>()
    var newBubble: BubbleB?

    func prepareNewBubbleForLaunch() {
        newBubble = gameBubble()
        newBubble!.update(position: launchVector)
        let go = GameObject(x: 0, y: 0, body: newBubble!, view: nil, imageName: "bubble-blue.png")
        scene.add(object: go)
    }

    func fireBubble(direction: CGVector) {
        // add a game bubble to the world and fire it from bottom middle of screen
        newBubble?.update(velocity: direction.normalize()! * speedOfBubble)
        // we should wait a little bit before preparing, for now this will do
        prepareNewBubbleForLaunch()
    }

    func tap(recognizer: UITapGestureRecognizer) {
        let loc = recognizer.location(in: view)
        rotateArrowTo(locationInRecognizer: loc)
        let dirVector = CGVector(point: loc) - launchVector
        fireBubble(direction: dirVector)
    }

    func rotateArrowTo(locationInRecognizer loc: CGPoint) {
        let dirVec = CGVector(point: loc) - launchVector
        // y is pointing downwards
        let angleFromY = dirVec.angleFromY()
        // experiment results show that this bounds work well for the cannon from 1.8 to 4.48
        if angleFromY >= 1.8 && angleFromY <= 4.48 {
            let ct = CGAffineTransform(rotationAngle: angleFromY - CGFloat.pi)
            arrow.transform = ct
        }
    }

    func pan(recognizer: UIPanGestureRecognizer) {
        let loc = recognizer.location(in: self.view)
        rotateArrowTo(locationInRecognizer: loc)

        let dirVector = CGVector(point: loc) - launchVector
        if recognizer.state == .ended {
            fireBubble(direction: dirVector)
        }
    }

    func snap(notification: Notification) {
        guard let snapData = notification.object as? SnapData else {
            return
        }
        if snapData.collisionBody != nil {
            // collided body already has index path and position
            // we look at snap point relative to collided body position to build a new snap point
            let collidedBody = snapData.collisionBody as! BubbleB
            let collidedBodyPosition = collidedBody.state.position

            let snapPoint = snapData.snapPoint
            let isLeft = snapPoint.x < collidedBodyPosition.dx
            let isBelow = snapPoint.y > collidedBodyPosition.dy
            let yDelta = isBelow ? 1 : 0
            var xDelta = 0
            if collidedBody.indexPath!.section % 2 == 0 {
                // even rows have more bubbles
                xDelta = isLeft ? -1 : 0
            } else {
                xDelta = isLeft ? (isBelow ? 0 : -1) : (isBelow ? 0 : 1)
            }
            let newIndexPath = IndexPath(row: collidedBody.indexPath!.row + xDelta,
                                         section: collidedBody.indexPath!.section + yDelta)
            let b = snapData.body as! BubbleB
            b.indexPath = newIndexPath
            bubbleGrid[newIndexPath.section][newIndexPath.row] = b
            burstCondition.grid = bubbleGrid
            let bc = burstCondition.determineBurst(indexPath: newIndexPath)
            for ip in bc {
                let bubbleB = bubbleGrid[ip.section][ip.row]
                for go in scene.gameObjects {
                    if go.body === bubbleB {
                        scene.remove(object: go)
                    }
                }
                bubbleGrid[ip.section][ip.row] = nil
            }
        } else if snapData.collisionEdge != nil {
            // collide with top edge
            let newRow = Int(snapData.snapPoint.x / (bubbleWidth))
            burstCondition.grid = bubbleGrid
            let newIndexPath = IndexPath(row: newRow, section: 0)
            let b = snapData.body as! BubbleB
            bubbleGrid[0][newRow] = b
            b.indexPath = newIndexPath
            let bc = burstCondition.determineBurst(indexPath: newIndexPath)
        }

    }

    func gameBubble() -> BubbleB {
        return BubbleB(indexPath: nil, radius: self.bubbleRadius, kind: GameBubble.Kind.Blue)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let game = Game(size: self.view.frame.size, rootView: self.view)
        scene = Scene(physics: game.physics)
        game.add(scene: scene, setCurrentSceneToThis: true)
        world = game.physics

        let gameWidth = self.view.frame.width
        let gameHeight = self.view.frame.height
        let numBubblesPerRow: CGFloat = 12
        bubbleWidth = gameWidth / numBubblesPerRow
        bubbleRadius = bubbleWidth / 2
        overlap = bubbleRadius / 4
        launchVector = CGVector(dx: gameWidth / 2, dy: gameHeight - 100)

        let uiTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tap(recognizer:)))
        view.addGestureRecognizer(uiTapGestureRecognizer)

        let impulseBehavior = ImpulseBehavior()
        world.add(behavior: impulseBehavior)

        let edgeCollisionBehavior = EdgeCollisionBehavior(bounds: self.view.bounds)
        world.add(behavior: edgeCollisionBehavior)

        let bodyCollisionBehavior = BodyCollisionBehavior()
        world.add(behavior: bodyCollisionBehavior)

        var snapGrid: [CGPoint] = Array(1...3).map { i in
            return CGPoint(x: bubbleWidth * CGFloat(i) + bubbleRadius, y: bubbleRadius)
        }
        snapGrid.append(contentsOf: Array(0...10).map { i in
            return CGPoint(x: bubbleWidth * CGFloat(i) + bubbleWidth, y: bubbleWidth + bubbleRadius - overlap)
        })

        func blue(section: Int, row: Int) -> BubbleB {
            return BubbleB(
                indexPath: IndexPath(row: row, section: section), radius: bubbleRadius, kind: GameBubble.Kind.Blue)
        }

        bubbleGrid = TestHelper.gridToBubble(grid: TestHelper.rawGrid, radius: bubbleRadius)
        burstCondition.grid = bubbleGrid

        for section in 0..<bubbleGrid.count {
            for row in 0..<bubbleGrid[section].count {
                let initialX: CGFloat! = section % 2 == 0 ? bubbleRadius : bubbleWidth
                let point = CGPoint(x: initialX + (bubbleWidth * CGFloat(row)),
                                    y: bubbleRadius + (bubbleWidth - overlap) * CGFloat(section))
                snapGrid.append(point)
                guard let bubble = bubbleGrid[section][row] else {
                    continue
                }

                bubble.update(position: CGVector(dx: point.x, dy: point.y))
                bubble.treatment = .Static

                let go = GameObject(x: bubble.state.position.dx, y: bubble.state.position.dy,
                           body: bubble, view: nil, imageName: bubble.imageName)
                scene.add(object: go)
            }
        }

        let snapBehavior = SnapBehavior(grid: snapGrid)

        world.add(behavior: snapBehavior)
        NotificationCenter.default.addObserver(
            self, selector: #selector(snap(notification:)), name: Constants.SnapToGrid, object: nil)

        // hack to make rotation around the bottom of the arrow image
        arrow = UIView(frame: CGRect(x: launchVector.dx - 30, y: launchVector.dy - 64, width: 60, height: 120))
        let arrowImage = UIImageView(image: UIImage(named: "arrow.png"))
        arrowImage.frame = CGRect(x: 0, y: 0, width: 60, height: 50)
        arrow.addSubview(arrowImage)
        view.addSubview(arrow)

        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(pan(recognizer:)))
        self.view.addGestureRecognizer(panRecognizer)
        prepareNewBubbleForLaunch()
        game.start()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
