//
//  BodyCollisionBehavior.swift
//  Physics
//
//  Created by Zhi An Ng on 8/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import CoreGraphics
import Foundation

/**
 Checks for collisions between all bodies.
 Emits a "collision:body" notification.
 */
class BodyCollisionBehavior: Behavior {

    override func connect() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(behave(data:)),
                                               name: Constants.IntegrateVelocity, object: nil)
    }

    override func disconnect() {
        NotificationCenter.default.removeObserver(self, name: Constants.IntegrateVelocity, object: nil)
    }

    func checkCollisionBetween(circleA: CircleB, circleB: CircleB) -> CollisionData? {
        // 2 circles collide when their bounds overlap
        // i.e. when their centers (position) is apart by less than both radius
        let posA = circleA.state.position
        let posB = circleB.state.position
        let distanceBetween = (posA - posB).magnitude()
        let minimumDistanceToAvoidCollision = circleA.radius + circleB.radius

        if distanceBetween <= minimumDistanceToAvoidCollision {
            // TODO the normal vector doesn't matter right now cos we don't do impulse for collision between bodies
            return CollisionData(bodyA: circleA, bodyB: circleB, normal: CGVector(dx: 0, dy: 0))
        } else {
            return nil
        }
    }

    func checkCollisionBetween(bodyA: Body, bodyB: Body) -> CollisionData? {
        // TODO now only support colliding between 2 CircleB
        guard let circleA = bodyA as? CircleB, let circleB = bodyB as? CircleB else {
            return nil
        }
        return checkCollisionBetween(circleA: circleA, circleB: circleB)
    }

    // Checks the collision mask to ensure that both bodies should be able to collide
    func shouldCheckCollisionBetween(bodyA: Body, bodyB: Body) -> Bool {
        return bodyA.collisionMask & bodyB.collisionMask != 0
    }

    override func behave(data: Any) {
        // make this faster by checking only dynamic items with static items
        let dynamicBodies = self.getTargets().filter {
            $0.treatment == .Dynamic
        }
        let staticBodies = self.getTargets().filter {
            $0.treatment == .Static
        }
        // TODO make a better implementation that doesnt check all bodies
        for bodyA in dynamicBodies {
            for bodyB in staticBodies {
                if bodyA === bodyB {
                    // don't check collision between same bodies
                    continue
                }
                if bodyA.treatment == .Static && bodyB.treatment == .Static {
                    // if both are static, no point checking
                    continue
                }
                if !shouldCheckCollisionBetween(bodyA: bodyA, bodyB: bodyB) {
                    // don't check collision if masks don't match
                    continue
                }
                if let collisionData = checkCollisionBetween(bodyA: bodyA, bodyB: bodyB) {
                    NotificationCenter.default.post(name: Constants.CollisionBody, object: collisionData)
                }
            }
        }
    }
}
