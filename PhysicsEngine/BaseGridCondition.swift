//
//  BaseGridCondition.swift
//  Physics
//
//  Created by Zhi An Ng on 12/2/17.
//  Copyright © 2017 Zhi An Ng. All rights reserved.
//

import Foundation

class BaseGridCondition<T> {
    var grid: [[T?]] = []
    // each node can have 6 possible neighbours, 2 above, 2 beside, 2 below
    // * * * *
    //  * * *
    // * * * *
    //  * * *
    // in terms of indexPaths deltas, the possible neighbours depends on which row (section) the node is
    // if the section is odd
    // [ (section: -1, row: 0), (section: -1, row: +1),
    //   (section: 0, row: -1), (section: 0, row: +1),
    //   (section: +1, row: 0), (section: +1, row: +1), ]
    // if the section is even
    // [ (section: -1, row: -1), (section: -1, row: 0),
    //   (section: 0, row: -1), (section: 0, row: +1),
    //   (section: +1, row: -1), (section: +1, row: 0), ]
    var oddIndexDeltas = [
        IndexPathDelta(sectionDelta: -1, rowDelta: 0),
        IndexPathDelta(sectionDelta: -1, rowDelta: +1),
        IndexPathDelta(sectionDelta: 0, rowDelta: -1),
        IndexPathDelta(sectionDelta: 0, rowDelta: +1),
        IndexPathDelta(sectionDelta: +1, rowDelta: 0),
        IndexPathDelta(sectionDelta: +1, rowDelta: +1),
    ]
    var evenIndexDeltas = [
        IndexPathDelta(sectionDelta: -1, rowDelta: -1),
        IndexPathDelta(sectionDelta: -1, rowDelta: 0),
        IndexPathDelta(sectionDelta: 0, rowDelta: -1),
        IndexPathDelta(sectionDelta: 0, rowDelta: +1),
        IndexPathDelta(sectionDelta: +1, rowDelta: -1),
        IndexPathDelta(sectionDelta: +1, rowDelta: 0),
    ]

    func deltas(for indexPath: IndexPath) -> [IndexPathDelta] {
        return indexPath.section % 2 == 0 ? evenIndexDeltas : oddIndexDeltas
    }

    // check if this indexPath is in the grid
    func indexPathValid(indexPath: IndexPath) -> Bool {
        return
            indexPath.section >= 0 && indexPath.row >= 0 &&
            grid.count > indexPath.section && grid[indexPath.section].count > indexPath.row &&
            grid[indexPath.section][indexPath.row] != nil
    }

    // returns all index path of neighbours that are valid (exist in the grid)
    func neighbouringIndexPaths(indexPath: IndexPath) -> [IndexPath] {
        var neighbours: [IndexPath] = []
        for delta in deltas(for: indexPath) {
            let neighbourIndexPath = indexPath + delta
            if indexPathValid(indexPath: neighbourIndexPath) {
                neighbours.append(neighbourIndexPath)
            }
        }
        return neighbours
    }

    func nodeAt(_ indexPath: IndexPath) -> T? {
        return grid[indexPath.section][indexPath.row]
    }

    // Runs DFS on grid starting at path.
    // An optional stopCondition can be specified to stop going down the current path, defaults to false, which means to
    // pursue every possible path.
    // An optional action can be specified to be run on each node.
    func dfs(path: IndexPath, stopCondition: ((T, IndexPath) -> Bool)?, action: ((T, IndexPath) -> Void)?) {
        var visited: Set<IndexPath> = Set()

        func helper(_ path: IndexPath) {
            // ensure that node is not nil and has not been visited
            guard let node = nodeAt(path), !visited.contains(path) else {
                return
            }

            if stopCondition?(node, path) ?? false {
                return
            }

            action?(node, path)

            visited.insert(path)

            for n in neighbouringIndexPaths(indexPath: path) {
                helper(n)
            }
        }

        helper(path)
    }

}
